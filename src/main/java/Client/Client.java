package Client;

import io.grpc.Channel;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import proto.Person;
import proto.PersonServiceGrpc;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {

    private static final Logger logger = Logger.getLogger(Client.class.getName());

    private final PersonServiceGrpc.PersonServiceBlockingStub blockingStub;

    public Client(Channel channel) {
        blockingStub = PersonServiceGrpc.newBlockingStub(channel);
    }

    public static void main(String[] args) throws Exception {
        String target = "localhost:50051";

        ManagedChannel channel = ManagedChannelBuilder
                    .forTarget(target)
                    .usePlaintext()
                    .build();

        boolean onClient = true;
        while (onClient) {
            Scanner scan = new Scanner(System.in);

            System.out.println("Name: ");
            String name = scan.next();
            System.out.println("CNP: ");
            String CNP = scan.next();

            Client client = new Client(channel);
            client.personsInfo(name, CNP);
        }
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    public void personsInfo(String name, String CNP) {
        logger.info("Will try to extract age and gender of " + name + "...");

        Person.DataRequest request = Person.DataRequest
                                .newBuilder()
                                .setName(name)
                                .setCnp(CNP)
                                .build();
        Person.DataResponse response;
        try {
            response = blockingStub.getData(request);
        } catch (StatusRuntimeException exception) {
            logger.log(Level.WARNING, "RPC failed: " , exception.getStatus());
            return;
        }
        logger.info("Hello, " + response);
    }
}
