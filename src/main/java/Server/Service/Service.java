package Server.Service;

import io.grpc.stub.StreamObserver;
import proto.Person;
import proto.PersonServiceGrpc;

import java.time.LocalDate;
import java.time.Period;

public class Service extends PersonServiceGrpc.PersonServiceImplBase
{

    public String getBirthDate (String CNP) {
        return CNP.substring(1, 7);
    }

    public String extractAge (String CNP) {
        String birthDate = getBirthDate(CNP);
        String year = birthDate.substring(0, 2);
        if (extractSecol(CNP) == 0) {
            year = "19" + year;
        } else {
            year = "20" + year;
        }

        LocalDate localDate = LocalDate.of(Integer.parseInt(year), Integer.parseInt(birthDate.substring(2, 4)), Integer.parseInt(birthDate.substring(4, 6)));
        LocalDate now = LocalDate.now();
        Period difference = Period.between(localDate, now);

        return Integer.toString(difference.getYears());
    }

    public Integer extractSecol (String CNP) {
        if (CNP.charAt(0) == '1' || CNP.charAt(0) == '2') {
            return 0;
        } else {
            return 1;
        }
    }

    public String extractGender (String CNP) {
        if (CNP.charAt(0) == '1' || CNP.charAt(0) == '5') {
            return "MASCULIN";
        } else if (CNP.charAt(0) == '2' || CNP.charAt(0) == '6') {
            return "FEMININ";
        } else {
            return "Too old for this world";
        }
    }

    @Override
    public void getData(Person.DataRequest request, StreamObserver<Person.DataResponse> responseObserver)
    {
        Person.DataResponse response = Person.DataResponse
                .newBuilder()
                .setName((request.getName()))
                .setGender(extractGender(request.getCnp()))
                .setAge(extractAge(request.getCnp()))
                .build();
        System.out.println("Name: " + request.getName() + ", gender: " + response.getGender() +
                ", age: " + response.getAge());
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
