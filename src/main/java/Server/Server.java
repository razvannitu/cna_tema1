package Server;

import Server.Service.Service;
import io.grpc.ServerBuilder;

import java.io.IOException;
import java.util.logging.Logger;

public class Server {

    private io.grpc.Server server;

    private static final Logger logger = Logger.getLogger(Server.class.getName());

    public Server() {
        int port = 50051;
        server = ServerBuilder
                .forPort(port)
                .addService(new Service())
                .build();
    }

    public void start() throws Exception {
        server.start();
        logger.info("Server started, listening on port " + server.getPort());

        try {
            server.awaitTermination();
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

        System.err.println("Server shut down");
    }
}
